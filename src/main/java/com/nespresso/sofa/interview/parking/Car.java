package com.nespresso.sofa.interview.parking;

/**
 * Created by bernhard on 13.04.17.
 */
class Car {

    private final boolean disabledDriver;
    private final char carType;

    Car(char charType) {
        this.carType = charType;
        this.disabledDriver = charType == 'D';
    }

    boolean isDisabledDriver() {
        return disabledDriver;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(carType).toString();
    }
}
