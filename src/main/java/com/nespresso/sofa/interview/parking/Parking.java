package com.nespresso.sofa.interview.parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the parking mechanisms: park/unpark a car (also for disabled-only bays) and provides a string representation of its state.
 */
public class Parking {

    private final List<PedestrianExit> exits;
    private final List<Slot> slots;
    private final int squareSize;

    Parking(List<PedestrianExit> exits, List<Slot> slots, int squareSize) {
        this.exits = exits;
        this.slots = slots;
        this.squareSize = squareSize;
    }

    /**
     * @return the number of available parking bays left
     */
    public int getAvailableBays() {
        int available = 0;
        for (Slot slot : slots) {
            if (slot.isAvailable()) {
                ++available;
            }
        }
        return available;
    }

    /**
     * Park the car of the given type ('D' being dedicated to disabled people) in closest -to pedestrian exit- and first (starting from the parking's entrance)
     * available bay. Disabled people can only park on dedicated bays.
     *
     *
     * @param carType
     *            the car char representation that has to be parked
     * @return bay index of the parked car, -1 if no applicable bay found
     */
    public int parkCar(final char carType) {

        final Car car = new Car(carType);

        final List<BaySearch> searches = new ArrayList<>();
        for (PedestrianExit exit : exits) {
            final BaySearch baySearch = new BaySearch(slots, exit.getIdx(), car);
            searches.add(baySearch);
        }

        boolean anyHasMore = true;
        while(anyHasMore) {

            anyHasMore = false;
            for (BaySearch currentSearch : searches) {

                final Integer found = currentSearch.next();
                if (found != null) {
                    this.slots.get(found).park(car);
                    return found;
                }
                anyHasMore = anyHasMore || currentSearch.mayProceed();
            }

        }

        return -1;
    }

    /**
     * Unpark the car from the given index
     *
     * @param index
     * @return true if a car was parked in the bay, false otherwise
     */
    public boolean unparkCar(final int index) {
        final Slot slot = slots.get(index);
        return slot.unparkCar();
    }

    /**
     * Print a 2-dimensional representation of the parking with the following rules:
     * <ul>
     * <li>'=' is a pedestrian exit
     * <li>'@' is a disabled-only empty bay
     * <li>'U' is a non-disabled empty bay
     * <li>'D' is a disabled-only occupied bay
     * <li>the char representation of a parked vehicle for non-empty bays.
     * </ul>
     * U, D, @ and = can be considered as reserved chars.
     *
     * Once an end of lane is reached, then the next lane is reversed (to represent the fact that cars need to turn around)
     *
     * @return the string representation of the parking as a 2-dimensional square. Note that cars do a U turn to continue to the next lane.
     */
    @Override
    public String toString() {
        final StringBuilder repr = new StringBuilder();
        for (int lane = 0; lane < squareSize; ++lane) {
            final boolean even = lane % 2 == 0;
            final StringBuilder laneRepr = new StringBuilder();
            for (int laneIdx = 0; laneIdx < squareSize; ++laneIdx) {
                final String curr = slots.get(lane * squareSize + laneIdx).toString();
                if (even) {
                    laneRepr.append(curr);
                } else {
                    laneRepr.insert(0, curr);
                }
            }
            repr.append(laneRepr);
            if (lane + 1 < squareSize) {
                repr.append("\n");
            }
        }
        return repr.toString();
    }

}
