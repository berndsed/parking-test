package com.nespresso.sofa.interview.parking;

/**
 * Created by bernhard on 13.04.17.
 */
abstract class Slot {

    abstract boolean park(Car car);
    abstract boolean unparkCar();
    abstract boolean isAvailable();

}
