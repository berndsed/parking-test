package com.nespresso.sofa.interview.parking;

/**
 * Created by bernhard on 13.04.17.
 */
class Bay extends Slot {

    static Bay newBay() {
        return new Bay(false);
    }

    static Bay newDisabledOnlyBay() {
        return new Bay(true);
    }

    private final boolean disabledOnly;

    private Bay(boolean disabledOnly) {
        this.disabledOnly = disabledOnly;
    }

    private Car occupant;

    @Override
    boolean park(Car car) {
        if (car == null) {
            throw new IllegalArgumentException("null");
        }

        if (isOccupied()) {
            return false;
        } else if (disabledOnly == car.isDisabledDriver()) {
            this.occupant = car;
            return true;
        } else {
            return false;
        }

    }

    @Override
    boolean unparkCar() {
        final boolean emptied = isOccupied();
        occupant = null;
        return emptied;
    }

    boolean isOccupied() {
        return occupant != null;
    }

    @Override
    boolean isAvailable() {
        return !isOccupied();
    }

    @Override
    public String toString() {
        if (isOccupied()) {
            return occupant.toString();
        } else {
            return disabledOnly ? "@" : "U";
        }
    }
}
