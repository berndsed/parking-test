package com.nespresso.sofa.interview.parking;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by bernhard on 13.04.17.
 */
class BaySearch {

    private final ListIterator<Slot> left;
    private final ListIterator<Slot> right;
    private final Car car;

    BaySearch(List<Slot> slots, int exitIdx, Car car) {
        this.left = slots.listIterator(exitIdx);
        this.right = slots.listIterator(exitIdx);
        if (right.hasNext()) {
            right.next();
        }
        this.car = car;
    }

    boolean mayProceed() {
        return left.hasPrevious() || right.hasNext();
    }

    Integer next() {
        if (!mayProceed()) {
            return null;
        }

        if (left.hasPrevious()) {
            final int idx = left.previousIndex();
            Slot leftSlot = left.previous();
            if (leftSlot.park(car)) {
                return idx;
            }
        }

        if (right.hasNext()) {
            final int idx = right.nextIndex();
            Slot rightSlot = right.next();
            if (rightSlot.park(car)) {
                return idx;
            }
        }

        return null;

    }

}
