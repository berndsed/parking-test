package com.nespresso.sofa.interview.parking;

/**
 * Created by bernhard on 13.04.17.
 */
class PedestrianExit extends Slot implements Comparable<PedestrianExit> {

    private int idx;

    PedestrianExit(int location) {
        this.idx = location;
    }

    int getIdx() {
        return idx;
    }

    @Override
    boolean park(Car car) {
        return false;
    }

    @Override
    boolean isAvailable() {
        return false;
    }

    @Override
    boolean unparkCar() {
        return false;
    }

    @Override
    public int compareTo(PedestrianExit o) {
        return idx - o.idx;
    }

    @Override
    public String toString() {
        return "=";
    }
}
