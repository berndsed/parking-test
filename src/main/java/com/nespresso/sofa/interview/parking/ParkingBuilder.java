package com.nespresso.sofa.interview.parking;

import java.util.*;

/**
 * Builder class to get a parking instance
 */
public class ParkingBuilder {

    private int squareSize;

    public ParkingBuilder withSquareSize(final int size) {
        if (size <= 0) {
            throw new IllegalArgumentException(Integer.toString(size));
        }
        this.squareSize = size;
        return this;
    }

    private final Collection<Builder> allSlotBuilders = new ArrayList<>();
    private final Collection<PedestrianExitBuilder> allExitBuilders = new ArrayList<>();


    public ParkingBuilder withPedestrianExit(final int pedestrianExitIndex) {
        final PedestrianExitBuilder builder = new PedestrianExitBuilder(pedestrianExitIndex);
        this.allSlotBuilders.add(builder);
        this.allExitBuilders.add(builder);
        return this;
    }

    public ParkingBuilder withDisabledBay(final int disabledBayIndex) {
        this.allSlotBuilders.add(new DisabledBayBuilder(disabledBayIndex));
        return this;
    }

    public Parking build() {
        final int length = squareSize * squareSize;
        final List<Slot> slots = new ArrayList<>();
        for (int idx = 0; idx < length; ++idx) {
            slots.add(Bay.newBay());
        }

        for (Builder builder : allSlotBuilders) {
            builder.build(slots);
        }

        final List<PedestrianExit> exits = new ArrayList<>();
        for (PedestrianExitBuilder exitBuilder : allExitBuilders) {
            if (exitBuilder.built != null) {
                exits.add(exitBuilder.built);
            }
        }
        Collections.sort(exits);

        return new Parking(exits, slots, squareSize);
    }

    private abstract static class Builder {

        final int location;

        private Builder(int location) {
            if (location < 0) {
                throw new IllegalArgumentException(Integer.toString(location));
            }
            this.location = location;
        }

        abstract void build(List<Slot> into);

    }

    private static class PedestrianExitBuilder extends Builder {

        private PedestrianExit built;

        PedestrianExitBuilder(int pedestrianExitIndex) {
            super(pedestrianExitIndex);
        }

        @Override
        void build(List<Slot> into) {
            if (location < into.size()) {
                this.built = new PedestrianExit(location);
                into.set(location, this.built);
            }
        }
    }

    private static class DisabledBayBuilder extends Builder {

        private DisabledBayBuilder(int location) {
            super(location);
        }

        @Override
        void build(List<Slot> into) {
            if (location < into.size()) {
                into.set(location, Bay.newDisabledOnlyBay());
            }
        }
    }

}
